<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;

class ConverterController extends Controller
{
    
    function convert(Request $request){
        $this->validate($request, [
            'file' => 'required|mimes:xls,xlsx'
        ]);
        $name = $request->file('file')->getClientOriginalName();

        $range = ($request->has('page')) ? $request->page : false;
        
        $request->file('file')->move('file', $name);
        $this->convertToPdf($name, $range);
        $filename = pathinfo($request->file('file')->getClientOriginalName(), PATHINFO_FILENAME);
        $pdf = $filename.".pdf";
        return response()->json(['file'=>$pdf]);
    }

    function convertToPdf($name, $page=false){
        if($page){
            $pageRange = " -e PageRange=$page ";
        }else{
            $pageRange="";
        }
        $process = new Process("unoconv -f pdf ".$pageRange."/var/www/html/converter-web-api/public/file/".$name."");
        $process->run();

        if (!$process->isSuccessful()) {
            throw new ProcessFailedException($process);
        }
    }
}
